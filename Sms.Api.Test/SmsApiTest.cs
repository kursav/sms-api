using NUnit.Framework;
using static Sms.Api.DataContracts;

namespace Sms.Api.Test
{
    [TestFixture]

    public class SmsApiTest
    {


        [SetUp]
        public void Init()
        {

        }


        [Test]
        [Repeat(2)]
        [Parallelizable]
        public void SendSms_1()
        {
            Sms sms = new Sms();
            sms.PhoneNumber = "00436607698672";
            sms.Message = "Test";
            var response = WebRequest.Create("http://192.168.1.100:4876/send/", "192.168.1.100:4876", HttpMethod.POST, ContractType.Json)
                      //.Authorization("", HttpAuthorizationType.Basic)
                      .Body(sms)
                      .StartHttpRequest<Sms>();
            Assert.IsTrue(response.GetType() == typeof(Sms));
        }


        [Test]
        [Repeat(2)]
        [Parallelizable]
        public void SendSms_2()
        {
            var response = WebRequest.Create("http://192.168.1.100:4876/send/", "192.168.1.100:4876", HttpMethod.POST, ContractType.Json)
                    //.Authorization("", HttpAuthorizationType.Basic)
                    .StartHttpRequest<Sms>();
            Assert.IsTrue(response.GetType() == typeof(Sms));
        }
    }
}