﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Sms.Api
{
    public static class DataContracts
    {
        static Lazy<Dictionary<ContractType, MediaTypeFormatter>> Formatter = null;
        static DataContracts()
        {
            Formatter = new Lazy<Dictionary<ContractType, MediaTypeFormatter>>(() => AddFormater());
        }
        private static Dictionary<ContractType, MediaTypeFormatter> AddFormater()
        {
            Dictionary<ContractType, MediaTypeFormatter> formatter = new Dictionary<ContractType, MediaTypeFormatter>();
            formatter.Add(ContractType.Json, new JsonMediaTypeFormatter());
            formatter.Add(ContractType.Xml, new XmlMediaTypeFormatter());
            formatter.Add(ContractType.Bson, new BsonMediaTypeFormatter());

            return formatter;

        }
        public enum ContractType
        {
            Json,
            Xml,
            Bson
        }
        public static string Serialize<T>(this T obj)
        {
            var ms = new MemoryStream();
            // Write an object to the Stream and leave it opened
            using (var writer = XmlDictionaryWriter.CreateTextWriter(ms, Encoding.UTF8, ownsStream: false))
            {
                var ser = new DataContractSerializer(typeof(T));
                ser.WriteObject(writer, obj);
            }
            // Read serialized string from Stream and close it
            using (var reader = new StreamReader(ms, Encoding.UTF8))
            {
                ms.Position = 0;
                return reader.ReadToEnd();
            }
        }

        public static string Serialize<T>(this T value, ContractType type)
        {
            if (typeof(T) == typeof(string))
            {
                return String.Format("{{0}}", value.ToString());
            }
            // Create a dummy HTTP Content.
            Stream stream = new MemoryStream();
            var content = new StreamContent(stream);
            /// Serialize the object.
            MediaTypeFormatter mediaTypeFormatter = Formatter.Value[type];

            mediaTypeFormatter.WriteToStreamAsync(typeof(T), value, stream, content, null).Wait();
            // Read the serialized string.
            stream.Position = 0;
            return content.ReadAsStringAsync().Result;
        }
        private class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding { get { return Encoding.UTF8; } }
        }
        public static T DeserializeFromFile<T>(this string path, ContractType type) where T : class
        {
            bool result = File.Exists(path);
            if (result)
            {
                if (type == ContractType.Json)
                {
                    using (StreamReader r = new StreamReader(path))
                    {
                        var json = r.ReadToEnd();
                        byte[] byteArray = Encoding.ASCII.GetBytes(json);
                        MemoryStream stream = new MemoryStream(byteArray);
                        var formatter = new JsonMediaTypeFormatter();
                        return formatter.ReadFromStreamAsync(typeof(T), stream, null, null).Result as T;
                    }
                }

            }
            return null;
        }

        public static T DeserializeFromString<T>(this string str, ContractType type) where T : class
        {
            if (type == ContractType.Json)
            {
                byte[] byteArray = Encoding.ASCII.GetBytes(str);
                MemoryStream stream = new MemoryStream(byteArray);
                var formatter = new JsonMediaTypeFormatter();
                return formatter.ReadFromStreamAsync(typeof(T), stream, null, null).Result as T;
            }
            return null;
        }

        public static T Deserialize<T>(this Stream stream, ContractType type) where T : class
        {

            MediaTypeFormatter mediaTypeFormatter = Formatter.Value[type];
            return mediaTypeFormatter.ReadFromStreamAsync(typeof(T), stream, null, null).Result as T;
        }

        public static string RemoveWhitespace(this string input)
        {
            return new string(input.ToCharArray()
                .Where(c => !Char.IsWhiteSpace(c))
                .ToArray());
        }


    }

}
