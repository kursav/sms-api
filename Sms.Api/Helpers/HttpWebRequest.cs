﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using static Sms.Api.DataContracts;

namespace Sms.Api
{
    public enum HttpMethod
    {
        GET,
        POST,
        PUT,
        DELETE
    }

    public enum HttpAuthorizationType
    {
        Basic,
        Bearer
    }

    public sealed class WebRequest
    {
        string _userAgent = @"Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";
        string _contentType;
        string _mediaType;
        string _accept;
        int _timeout = Convert.ToInt32(new TimeSpan(0, 2, 0).TotalMilliseconds);
        bool _keepAlive = false;
        MediaTypeFormatter _formatter;
        HttpWebRequest request;
        object _body;
        ContractType _contractType;
        private WebRequest(string uri, string host, HttpMethod method, ContractType contractType = ContractType.Json)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            request = (HttpWebRequest)System.Net.WebRequest.Create(uri);
            request.Host = host;
            request.UserAgent = _userAgent;
            request.Timeout = _timeout;
            request.Method = Enum.GetName(typeof(HttpMethod), method);
            request.KeepAlive = _keepAlive;
            _contractType = contractType;
            switch (contractType)
            {
                case ContractType.Json:
                    request.ContentType = _contentType = "application/json";
                    request.MediaType = _mediaType = "application/json";
                    request.Accept = _accept = "application/json";
                    _formatter = new JsonMediaTypeFormatter();
                    break;
                case ContractType.Xml:
                    request.ContentType = _contentType = "application/xml";
                    request.MediaType = _mediaType = "application/xml";
                    request.Accept = _accept = "application/xml";
                    _formatter = new XmlMediaTypeFormatter();
                    break;
                case ContractType.Bson:
                    request.ContentType = _contentType = "application/bson";
                    request.MediaType = _mediaType = "application/bson";
                    request.Accept = _accept = "application/bson";
                    _formatter = new BsonMediaTypeFormatter();
                    break;
                default:
                    break;
            }

        }

        public static WebRequest Create(string uri, string host, HttpMethod method, ContractType contractType = ContractType.Json)
        {
            return new WebRequest(uri, host, method, contractType);
        }

        public WebRequest Body(object body)
        {
            _body = body;
            return this;
        }

        public WebRequest Authorization(string userCredantials, HttpAuthorizationType authtype)
        {
            request.Headers.Add("Authorization", Enum.GetName(typeof(HttpAuthorizationType), authtype) + " " + userCredantials);
            return this;
        }
        public WebRequest Formatter(MediaTypeFormatter formatter)
        {
            _formatter = formatter;
            return this;
        }

        public WebRequest UserAgent(string userAgent)
        {
            _userAgent = userAgent;
            return this;
        }



        public async Task<T> StartHttpRequestAsync<T>()
        {
            try
            {
                if (_body != null)
                {
                    string serializedData = _body.Serialize(_contractType);
                    var data = Encoding.ASCII.GetBytes(serializedData);
                    using (var stream = await request.GetRequestStreamAsync())
                    {
                        if (stream == null)
                        {
                            return default(T);
                        }
                        stream.Write(data, 0, data.Length);

                    }
                }
                else
                {
                    request.ContentLength = 0;
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (typeof(T) == typeof(bool))
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            return (T)(object)true;
                        }
                        return (T)(object)false;
                    }

                    WebHeaderCollection headers = response.Headers;
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        var returnType = await _formatter.ReadFromStreamAsync(typeof(T), dataStream, null, null);
                        return (T)returnType;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public T StartHttpRequest<T>()
        {
            try
            {
                if (HttpWebRequest.DefaultMaximumErrorResponseLength < int.MaxValue)
                    HttpWebRequest.DefaultMaximumErrorResponseLength = int.MaxValue;

                if (_body != null)
                {
                    string serializedData = _body.Serialize(_contractType);
                    var data = Encoding.ASCII.GetBytes(serializedData);
                    using (var stream = request.GetRequestStream())
                    {
                        if (stream == null)
                        {
                            return default(T);
                        }
                        stream.Write(data, 0, data.Length);

                    }
                }
                else
                {
                    request.ContentLength = 0;
                }
                request.KeepAlive = true;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (typeof(T) == typeof(bool))
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            return (T)(object)true;
                        }
                    }

                    WebHeaderCollection headers = response.Headers;
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        var returnType = _formatter.ReadFromStreamAsync(typeof(T), dataStream, null, null);
                        return (T)returnType.Result;
                    }
                }

            }
            catch (Exception)
            {
                return default(T);
            }
        }


    }
}
