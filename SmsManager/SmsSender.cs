﻿using System;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;

namespace SmsManager
{


    public sealed class SmsSender
    {

        private static readonly object obj = new object();
        ReaderWriterLockSlim locker = new ReaderWriterLockSlim();

        /*
        * Private constructor ensures that object is not
        * instantiated other than with in the class itself
        */
        private SmsSender()
        {

        }
        private static SmsSender instance = null;
        /*
         * public property is used to return only one instance of the class
         * leveraging on the private property
         */
        public static SmsSender GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (obj)
                    {
                        if (instance == null)
                            instance = new SmsSender();
                    }
                }
                return instance;
            }
        }


        /*
         * Public method which can be invoked through the singleton instance
         */
        public void SendSms(string phoneNumber, string message)
        {
            try
            {
                locker.EnterWriteLock();
                try
                {
                    using (SerialPort serialPort = new SerialPort("COM16"))
                    {

                        if (!serialPort.IsOpen)
                        {
                            serialPort.Open();
                        }
                        int length = phoneNumber.Length;
                        while (length < 17)
                        {
                            phoneNumber += "$";
                            length = length + 1;
                        }
                        string jsonMessage = "@" + phoneNumber + message;
                        serialPort.Write(jsonMessage);
                        Thread.Sleep(10000);
                    }

                }
                finally
                {
                    locker.ExitWriteLock();
                }

            }
            catch (InvalidOperationException) { }
            catch (UnauthorizedAccessException) { }
            catch (Exception) { throw; }





        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
